# 🅿️ URL Parser for NorthCube 

The project contains the implementation of URL and Query parsers. The output of algorithms is the map with defined keys and values. The language used for parser is `Swift`.

---

### 📃 Usage 

Clone or download the project from Gitlab. Open and run the project in XCode. You may predefined testcases in the end of the `main.swift` file. 

[Youtube Demo Video](https://www.youtube.com/watch?v=nzlv1qX_ICg&feature=youtu.be)

Create `Parser` object with URL calling `Parser("url")`.

Example 

```Swift
let parser = Parser("http://username:password@hostname:9000/path?arg=value#anchor")
```

Call `parseURL()` function to get map of parsed components.

Call `parseQuery("query")` function to get map of parsed query components.

```Swift
let parser = Parser("http://username:password@hostname:9000/path?arg=value#anchor")

let mapURL = parser.parseURL()

print("Map for URL:")
print(mapURL)

print("Map for Query:")
let mapQuery = parser.parseQuery("key1=value1&key2=value2")

print(mapQuery)
```

Access each component by name if needed

```Swift
print(parser.scheme)
print(parser.path)
```

<img onmouseover=alert(1) onmouseout="normalImg(this)" border="0" src="https://www.w3schools.io//images/file/markdown/logo.png" alt="Smiley" width="32" height="32">

🔹 Testcases for URL

+ http://username:password@hostname:9000/path?arg=value#anchor
+ http://username:password@hostname/path?arg=value#anchor
+ http://username@hostname/path?arg=value#anchor
+ http://username@hostname/patharg=value#anchor
+ http://username@hostname/path?arg=valueanchor
+ http://username@hostname/?arg=value#anchor
+ http://@hostname/?arg=valueanchor
+ http://@hostname/?arg=value#anchor
+ nc://username@hostname/?arg=value#anchor
+ ://username@hostname/?arg=value#anchor
+ :/username@hostname/?arg=valueanchor
+ :/username@hostname/arg=valueanchor
+ //username@hostname/arg=valueanchor
+ http//username@hostname/arg=valueanchor
+ http//username@hostname/?arg=valueanchor
+ http//@hostname/arg=valueanchor
+ //@hostname/?arg=value#anchor
+ @hostname/?arg=value#anchor
+ ?arg=value#anchor
+ arg=value#anchor
+ ?northcbue
+ #sleepcycle

🔸Testcases for Query

+ key1=value1&key2=value2
+ key1&key2=value2
+ key1&key2


