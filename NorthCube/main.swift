//
//  main.swift
//  NorthCube
//
//  Created by Atalyk Akash on 27.03.18.
//  Copyright © 2018 Mac. All rights reserved.
//

//This project is dedicated for URL parsing and parsing query items
//In order to see the result run the project and see the console log
//Bottom of the file contains possible testcases for URL and Query parsing

import Foundation

class Parser {
    
    //URL components
    var url = ""
    var scheme = ""
    var host = ""
    var port = ""
    var username = ""
    var password = ""
    var path = ""
    var query = ""
    var fragment = ""
    var isHost = false
    var isPath = false
    var isQuery = false
    
    //Initialize Parser with URL
    init(_ url: String) {
        self.url = url
    }
    
    //Parse URL into components
    func parseURL() -> [String : String] {
        
        //Initialize map
        var map: [String : String] = [:]
        
        //Copy URL
        var _url = self.url
        
        var components = _url.split(separator: ":", maxSplits: 1, omittingEmptySubsequences: true).map(String.init)
        
        //Get scheme (ex. http)
        scheme = components.count > 1 ? (components.first ?? "") : ""
        
        //Set new URL based on components
        _url = components.count > 1 ? (components.last ?? "") : (components.first ?? "")
        
        components = _url.components(separatedBy: "//")
        
        //Consider edge case when URL lacks scheme but has hostname
        let isValidHostname = self.url.components(separatedBy: "://").count < 2 && components.count > 1 && components.filter{ !$0.isEmpty }.count < 2
        
        _url = components.count > 1 ? (components.last ?? "") : (components.first ?? "")

        //Consider two cases of URL: with hostname and without hostname
        if (components.count > 1 && !scheme.isEmpty) || isValidHostname {
            
            //Find @ chacter to parse username and password if ones exist
            components = _url.split(separator: "@", maxSplits: 1, omittingEmptySubsequences: true).map(String.init)
            
            let _user = components.count > 1 ? (components.first ?? "") : ""
            let userComponent = _user.split(separator: ":", maxSplits: 1, omittingEmptySubsequences: true).map(String.init)
            
            //Set `user` component
            username = userComponent.first ?? ""
            //Set `pass` component
            password = (userComponent.count > 1 && (userComponent.last ?? "").count > 0) ? (userComponent.last ?? "") : ""
            
            _url = components.count > 1 ? (components.last ?? "") : (components.first ?? "")
            
            //Check for path in URL
            components = _url.split(separator: "/", maxSplits: 1, omittingEmptySubsequences: true).map(String.init)
            
            //Parse hostname if hostname was not set
            if !isHost {
                //Separate hostname and port
                let _port = components.count > 1 ? (components.first ?? "") : ""
                let _hostname = _port.split(separator: ":", maxSplits: 1, omittingEmptySubsequences: true).map(String.init)
                host = _hostname.first ?? ""
                port = (_hostname.count > 1 && (_hostname.last ?? "").count > 0) ? (_hostname.last ?? "") : ""
                //Check if host is set
                isHost = components.count > 1
            }
            
            _url = components.count > 1 ? (components.last ?? "") : (components.first ?? "")
            
            isPath = isHost
            
            _url = isPath ? "/" + _url : _url
            
            path = isHost ? _url : ""
            
            //Check for query in URL
            components = _url.split(separator: "?", maxSplits: 1, omittingEmptySubsequences: true).map(String.init)
            
            //Parse hostname if hostname was not set
            if !isHost {
                let _port = components.count > 1 ? (components.first ?? "") : ""
                let _hostname = _port.split(separator: ":", maxSplits: 1, omittingEmptySubsequences: true).map(String.init)
                host = _hostname.first ?? ""
                port = (_hostname.count > 1 && (_hostname.last ?? "").count > 0) ? (_hostname.last ?? "") : ""
                isHost = components.count > 1
            }

            _url = components.count > 1 ? (components.last ?? "") : (components.first ?? "")
            
            //Set `path` component
            path = (isPath && components.count > 1) ? (components.count > 1 ? (components.first ?? "") : "") : path
            isPath = !(isPath && components.count > 1)
            
            isQuery = components.count > 1
            query = isQuery ? components.last ?? "" : ""

            _url = components.count > 1 ? (components.last ?? "") : (components.first ?? "")
            
            //Check for fragment in URL
            components = _url.split(separator: "#", maxSplits: 1, omittingEmptySubsequences: true).map(String.init)
            
            //Parse hostname if hostname was not set
            if !isHost {
                let _port = components.count > 1 ? (components.first ?? "") : ""
                let _hostname = _port.split(separator: ":", maxSplits: 1, omittingEmptySubsequences: true).map(String.init)
                host = _hostname.first ?? ""
                port = (_hostname.count > 1 && (_hostname.last ?? "").count > 0) ? (_hostname.last ?? "") : ""
                isHost = components.count > 1
            }
            
            //Set `query` component
            query = (components.count > 1 && !isPath && isQuery) ? (components.first ?? "") : query
            //Set `fragment` component
            fragment = components.count > 1 ? (components.last ?? "") : ""
            
            //Set `path` component
            path = (components.count > 1 && isPath) ? (components.first ?? "") : path
            
        } else {
            
            _url = scheme.isEmpty ? self.url : _url
            
            //Subtract a scheme from URL if exists
            path = _url
            
            //Check for query in URL
            components = _url.split(separator: "?", maxSplits: 1, omittingEmptySubsequences: false).map(String.init)
            
            _url = components.count > 1 ? (components.last ?? "") : (components.first ?? "")
            
            //Set `path` component
            path = (components.count > 1) ? (components.first ?? "") : path
            //Set `query` component
            query = (components.count > 1) ? (components.last ?? "") : query
            isQuery = components.count > 1
            
            _url = components.count > 1 ? (components.last ?? "") : (components.first ?? "")
            
            //Check for fragment in URL
            components = _url.split(separator: "#", maxSplits: 1, omittingEmptySubsequences: false).map(String.init)
            
            path = isQuery ? path : (components.first ?? "")
            query = (components.count > 1 && isQuery) ? (components.first ?? "") : query
            fragment = components.count > 1 ? (components.last ?? "") : ""
        }
        
        //Map values for keys
        map["scheme"] = scheme
        map["host"] = host
        map["port"] = port
        map["user"] = username
        map["pass"] = password
        map["path"] = path
        map["query"] = query
        map["fragment"] = fragment
        
        return map
    }
    
    func parseQuery(_ query: String) -> [String : String] {
        
        var map: [String : String] = [:]
        
        //Separate query to items
        let items = query.split(separator: "&").map(String.init)
        
        for item in items {
            //Divide query item into key and value
            let components = item.split(separator: "=")
            let key = String(components.first ?? "")
            let value = String(components.count > 1 ? (components.last ?? "") : "")
            //Map values for keys
            map[key] = value
        }
        
        return map
    }
}

let parser = Parser("http://username:password@hostname:9000/path?arg=value#anchor")

let mapURL = parser.parseURL()

print("Map for URL:")
print(mapURL)

print("Map for Query:")
let mapQuery = parser.parseQuery("key1=value1&key2=value2")

print(mapQuery)

/* Testcases for URL Parser
http://username:password@hostname:9000/path?arg=value#anchor
http://username:password@hostname/path?arg=value#anchor
http://username@hostname/path?arg=value#anchor
http://username@hostname/patharg=value#anchor
http://username@hostname/path?arg=valueanchor
http://username@hostname/?arg=value#anchor
http://@hostname/?arg=valueanchor
http://@hostname/?arg=value#anchor
nc://username@hostname/?arg=value#anchor
://username@hostname/?arg=value#anchor
:/username@hostname/?arg=valueanchor
:/username@hostname/arg=valueanchor
//username@hostname/arg=valueanchor
http//username@hostname/arg=valueanchor
http//username@hostname/?arg=valueanchor
http//@hostname/arg=valueanchor
//@hostname/?arg=value#anchor
@hostname/?arg=value#anchor
?arg=value#anchor
arg=value#anchor
?northcbue
#sleepcycle
*/

/*Testcases for Query
key1=value1&key2=value2
key1&key2=value2
key1&key2
*/

//Thanks for watching:)

